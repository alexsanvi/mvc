<?php

class Model
{
    protected $conexion;

    public function __construct($dbname, $dbuser, $dbpass, $dbhost)
    {
        $mvc_bd_conexion = new mysqli($dbhost, $dbuser, $dbpass);
        $error = $mvc_bd_conexion->connect_errno;

        if ($error != null) {
            die('No ha sido posible realizar la conexión con la base de datos: ' .
                $mvc_bd_conexion->connect_error);
        }

        $mvc_bd_conexion->select_db($dbname);
        $mvc_bd_conexion->set_charset('utf8');
        $this->conexion = $mvc_bd_conexion;
    }

    private function dameAlimentosDB($sql)
    {
        $result = $this->conexion->query($sql);
        $alimentos = array();
        while ($row = $result->fetch_assoc())
        {
            $alimentos[] = $row;
        }

        return $alimentos;
    }

    public function dameAlimentos()
    {
        $sql = "select * from alimentos order by energia desc";
        return $this->dameAlimentosDB($sql);
    }
}