<?php


class Controller
{
    public function inicio()
    {
        $params = array(
            'mensaje' => 'Bienvenido a la aplicación de alimentos',
            'fecha' => date('d-m-y')
        );

        require __DIR__ . '/templates/inicio.php';
    }

    public function listar()
    {
        $m = new Model(
            Config::$mvc_bd_nombre, Config::$mvc_bd_usuario,
            Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $params = array('alimentos' => $m->dameAlimentos());

        require __DIR__ . '/templates/mostrarAlimentos.php';
    }
}